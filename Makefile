OBJECTS = main.o App.o GLFactory.o GL.o GLFunctions.o Exception.o

build: $(OBJECTS)
	@g++ $(OBJECTS) -lGL -lglut -lGLU -o rb

main.o: main.cpp include/SLC/GL/Factory.h include/SLC/App.h include/SLC/Exception.h
	@g++ -I./include -c main.cpp -o main.o

App.o: src/SLC/App.cpp include/SLC/App.h include/SLC/GL/GL.h include/SLC/Exception.h
	@g++ -I./include -c src/SLC/App.cpp -o App.o

GLFactory.o: src/SLC/GL/Factory.cpp include/SLC/GL/Factory.h include/SLC/GL/GL.h
	@g++ -I./include -c src/SLC/GL/Factory.cpp -o GLFactory.o

GL.o: src/SLC/GL/GL.cpp include/SLC/GL/GL.h include/SLC/Exception.h
	@g++ -I./include -c src/SLC/GL/GL.cpp -o GL.o

GLFunctions.o: src/SLC/GL/functions.cpp include/SLC/GL/functions.h
	@g++ -I./include -c src/SLC/GL/functions.cpp -o GLFunctions.o

Exception.o: src/SLC/Exception.cpp include/SLC/Exception.h
	@g++ -I./include -c src/SLC/Exception.cpp -o Exception.o

.PHONY: clean
clean:
	@rm -rf *.o rb
