#ifndef SLC_GL_GL_H
#define SLC_GL_GL_H

#include <SLC/Exception.h>
#include <SLC/GL/functions.h>

namespace SLC {
namespace GL {

class GL
{
public:
	GL(int* argc, char* argv[]);
};

}}

#endif