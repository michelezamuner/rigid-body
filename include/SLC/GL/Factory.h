#ifndef SLC_GL_FACTORY_H
#define SLC_GL_FACTORY_H

#include <SLC/GL/GL.h>

namespace SLC {
namespace GL {

class Factory
{
public:
	GL* create(int* argc, char* argv[]);
};

}
}

#endif