#ifndef SLC_GL_FUNCTIONS_H
#define SLC_GL_FUNCTIONS_H

#include <GL/freeglut.h>
#include <GL/gl.h>
#include <GL/glu.h>

#include <SLC/Exception.h>

namespace SLC {
namespace GL {

void display();
void idle();
void keyboard (unsigned char, int, int);

}}

#endif