#ifndef SLC_APP_H
#define SLC_APP_H

// #include <SLC/GL/GL.h>
#include <SLC/GL/Factory.h>
#include <SLC/Exception.h>

namespace SLC {

class App
{
public:
	// App(GL::GL* gl);
	App(GL::Factory* glFactory);
	void run(int* argc, char* argv[]);
private:
	// GL::GL* gl;
	GL::Factory* glFactory;
};

}

#endif