#ifndef SLC_EXCEPTION_H
#define SLC_EXCEPTION_H

#include <string>

using std::string;

namespace SLC {

class Exception
{
public:
	Exception(string);
	string getMessage();
private:
	string message;
};

}

#endif