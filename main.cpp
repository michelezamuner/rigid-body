#include <cstdlib>
#include <iostream>

// #include <SLC/GL/GL.h>
#include <SLC/GL/Factory.h>
#include <SLC/App.h>
#include <SLC/Exception.h>

// using ::SLC::GL::GL;
// using GLFactory = SLC::GL::Factory;
typedef SLC::GL::Factory GLFactory;
using SLC::App;
using SLC::Exception;
using std::cout;
using std::endl;

int main(int argc, char* argv[])
{
  int exit_code = EXIT_SUCCESS;

  try {
    // GL* gl = new GL(argc, argv);
    // GLFactory* glFactory = new GLFactory();
    // App* app = new App(gl);
    App* app = new App(new GLFactory);
    app->run(&argc, argv);
  } catch (Exception e) {
    cout << e.getMessage() << endl;
    exit_code = EXIT_FAILURE;
  }

  return exit_code;
}
