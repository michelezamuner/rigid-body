#include <SLC/App.h>

#include <iostream>

namespace SLC {

// App::App(GL::GL* gl)
// : gl(gl)
App::App(GL::Factory* glFactory)
: glFactory(glFactory)
{
	std::cout << "App::App()" << std::endl;
}

void App::run(int* argc, char* argv[])
{
	std::cout << "App::run()" << std::endl;
	GL::GL* gl = glFactory->create(argc, argv);
}

}