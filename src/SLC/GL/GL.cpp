#include <SLC/GL/GL.h>
#include <SLC/GL/functions.h>

#include <iostream>

namespace SLC {
namespace GL {

GL::GL(int* argc, char* argv[])
{
	::glutInit(argc, argv);
	::glutInitDisplayMode(GLUT_DOUBLE | GLUT_DEPTH);
	::glutInitWindowSize(500, 500);
	::glutInitWindowPosition(100, 100);
	::glutCreateWindow("test window");
	::glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	::glutDisplayFunc(display);

	::glutMainLoop();
}

}}
