#include <SLC/GL/functions.h>

namespace SLC {
namespace GL {

void display()
{
	::glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	::glutSwapBuffers();
}

void idle()
{
	throw Exception("GL Idle function");
}

void keyboard()
{

}

}}
