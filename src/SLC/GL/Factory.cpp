#include <SLC/GL/Factory.h>

#include <iostream>

namespace SLC {
namespace GL {

GL* Factory::create(int* argc, char* argv[])
{
	std::cout << "Factory::create()" << std::endl;
	return new GL(argc, argv);
}

}
}